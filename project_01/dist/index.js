class StatApp {
    constructor() {
        this.startApp();
    }
    startApp() {
        this.ui = new UI();
        this.stats = new Statistics();
        this.ui.setStatistics(this.stats);
        // this.ui.setInitialEvent();
    }
}
class UI {
    constructor() {
        this.nrInput = document.querySelector('#nr_input');
        this.sumInput = document.querySelector('#sum');
        this.avgInput = document.querySelector('#avg');
        this.minInput = document.querySelector('#min');
        this.maxInput = document.querySelector('#max');
        this.setInitialEvent();
    }
    setStatistics(stats) {
        this.statistics = stats;
    }
    setInitialEvent() {
        this.nrInput.addEventListener('input', () => this.updateDataInputs());
    }
    setEvents() {
        for (let i = 0; i < this.entryInputs.length; i++) {
            this.entryInputs[i].addEventListener('input', () => this.updateStats());
        }
    }
    getData() {
        let numbers = new Array();
        for (let i = 0; i < this.entryInputs.length; i++) {
            let num = +this.entryInputs[i].value;
            numbers[i] = num;
        }
        return numbers;
    }
    updateStats() {
        let stats = document.querySelector('#stats');
        let wait = document.querySelector('#waiting');
        this.statistics.pushData(this.getData());
        if (isNaN(this.statistics.getSum())) {
            stats.setAttribute("hidden", "");
            wait.removeAttribute("hidden");
        }
        else {
            stats.removeAttribute("hidden");
            wait.setAttribute("hidden", "");
        }
        this.sumInput.value = this.statistics.getSum().toString();
        this.avgInput.value = this.statistics.getAvg().toString();
        this.minInput.value = this.statistics.getMin().toString();
        this.maxInput.value = this.statistics.getMax().toString();
    }
    updateDataInputs() {
        let parrent = document.querySelector('#inputs');
        parrent.innerHTML = "";
        let nrInputs = +this.nrInput.value;
        for (let i = 0; i < nrInputs; i++) {
            parrent.appendChild(this.createInput());
        }
        this.entryInputs = document.querySelectorAll('#data');
        this.setEvents();
        let stats = document.querySelector('#stats');
        stats.setAttribute("hidden", "");
    }
    createInput() {
        let input = document.createElement('input');
        input.setAttribute('id', 'data');
        input.setAttribute('type', 'text');
        return input;
    }
}
class Statistics {
    pushData(data) {
        this.data = data;
    }
    getSum() {
        let count = 0;
        this.data.forEach(element => count += element);
        return count;
    }
    getAvg() {
        return this.getSum() / this.data.length;
    }
    getMin() {
        return Math.min(...this.data);
    }
    getMax() {
        return Math.max(...this.data);
    }
}
const statApp = new StatApp();
