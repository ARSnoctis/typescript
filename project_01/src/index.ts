class StatApp {

    public ui: UI;
    public stats: Statistics;
    public constructor() {
        this.startApp();
    }
    public startApp(): void {
        this.ui = new UI();
        this.stats = new Statistics();
        this.ui.setStatistics(this.stats);
    }
}

class UI {
    private sumInput: HTMLInputElement;
    private avgInput: HTMLInputElement;
    private minInput: HTMLInputElement;
    private maxInput: HTMLInputElement;
    private nrInput: HTMLInputElement;
    private entryInputs: NodeListOf<HTMLInputElement>;
    private statistics:Statistics;

    constructor() {
        this.nrInput = document.querySelector('#nr_input');
        this.sumInput = document.querySelector('#sum');
        this.avgInput = document.querySelector('#avg');
        this.minInput = document.querySelector('#min');
        this.maxInput = document.querySelector('#max');
        this.setInitialEvent();
    }

    public setStatistics(stats: Statistics): void {
        this.statistics = stats;
    }

    private setInitialEvent(): void {
        this.nrInput.addEventListener('input', () => this.updateDataInputs());
    }
    
    private setEvents(): void {
        for (let i = 0; i < this.entryInputs.length; i++) {
            this.entryInputs[i].addEventListener('input', () => this.updateStats())
        }
    }

    private getData(): number[] {
        let numbers = new Array();
        for (let i = 0; i < this.entryInputs.length; i++) {
            let num: number = +this.entryInputs[i].value
            numbers[i] = num;
        }
        return numbers;
    }

    public updateStats(): void {
        let stats = document.querySelector('#stats');
        let wait = document.querySelector('#waiting');
        this.statistics.pushData(this.getData());
        if (isNaN(this.statistics.getSum())) {
            stats.setAttribute("hidden","");
            wait.removeAttribute("hidden");
        } else {
            stats.removeAttribute("hidden");
            wait.setAttribute("hidden","");
        }
        this.sumInput.value = this.statistics.getSum().toString();
        this.avgInput.value = this.statistics.getAvg().toString();
        this.minInput.value = this.statistics.getMin().toString();
        this.maxInput.value = this.statistics.getMax().toString();
    }

    public updateDataInputs(): void {
        let parrent = document.querySelector('#inputs');
        parrent.innerHTML = "";
        let nrInputs: number = +this.nrInput.value;
        for (let i = 0; i < nrInputs; i++) {
            parrent.appendChild(this.createInput());
        }
        this.entryInputs = document.querySelectorAll('#data');
        this.setEvents();
        let stats = document.querySelector('#stats');
        stats.setAttribute("hidden","");
    }

    private createInput(): HTMLInputElement {
        let input = document.createElement('input');
        input.setAttribute('id', 'data');
        input.setAttribute('type', 'text');
        return input;
    }
}

class Statistics {
    private data: number[];
    public pushData(data: number[]) {
        this.data = data;
    }

    public getSum(): number {
        let count = 0;
        this.data.forEach(element => count += element);
        return count;
    }

    public getAvg(): number {
        return this.getSum()/this.data.length;
    }

    public getMin(): number {
        return Math.min( ...this.data );
    }

    public getMax(): number {
        return Math.max( ...this.data );
    }
    
}

const statApp = new StatApp();