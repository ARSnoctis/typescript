class BeatboxApp {
    public ui: UI;
    public player: Player;
    public recorder: Recorder;
    public constructor() {
        this.startApp();
    }
    startApp() {
        this.player = new Player();
        this.recorder = new Recorder(this.player);
        this.ui = new UI(this.player, this.recorder);
    }
}

class UI {
    //beatbox buttons
    private player : Player;
    private recorder : Recorder
    private boom :HTMLButtonElement = document.querySelector("button#boom");
    private clap :HTMLButtonElement = document.querySelector("button#clap");
    private hihat :HTMLButtonElement = document.querySelector("button#hihat");
    private kick :HTMLButtonElement = document.querySelector("button#kick");
    private openhat :HTMLButtonElement = document.querySelector("button#openhat");
    private ride :HTMLButtonElement = document.querySelector("button#ride");
    private snare :HTMLButtonElement = document.querySelector("button#snare");
    private tink :HTMLButtonElement = document.querySelector("button#tink");
    private tom :HTMLButtonElement = document.querySelector("button#tom");
    private buttonMap = new Map();

    //recording elements
    private channels = new Map();
    private interval;

    constructor(player: Player, recorder: Recorder) {
        this.player = player;
        this.recorder = recorder;
        this.buttonMap.set("7", this.boom);
        this.buttonMap.set("8", this.clap);
        this.buttonMap.set("9", this.hihat);
        this.buttonMap.set("4", this.kick);
        this.buttonMap.set("5", this.openhat);
        this.buttonMap.set("6", this.ride);
        this.buttonMap.set("1", this.snare);
        this.buttonMap.set("2", this.tink);
        this.buttonMap.set("3", this.tom);
        this.collectChannelControls();
        this.setKeyEvents();
        this.refreshStates(1); //set innitial state and refresh controls
        this.setRadioClickedEvent();
    }
    
    //collects channels' controlls
    public collectChannelControls(): void {
        for (let i = 1; i <= 4; i++) {
            let channel = new ChannelElements(i);
            let states = new ChannelState(false, false, false);
            channel.setStates(states);
            this.channels.set(i, channel);
        }
    }

    //blinks beatbox buton event
    public blinkBeatboxButton(key: string) {
        let button = this.buttonMap.get(key);
        button.classList.remove("btn-outline-primary");
        button.classList.add("btn-pressed")
        setTimeout(() => {
            button.classList.remove("btn-pressed");
            button.classList.add("btn-outline-primary");
        }, 50);
    }

    //set document events and for record buttons
    public setKeyEvents() : void {
        document.addEventListener('keypress', (ev) => this.onKeyDown(ev));
        for (let entry of this.buttonMap.entries()) {
            entry[1].onclick = () => this.blinkAndPlay(entry[0]);
        }
        for (let i = 1; i <= 4; i++) {
            this.channels.get(i).getRecordButton().onclick = (ev: MouseEvent) => this.recordAction(ev, i);
            this.channels.get(i).getPlayButton().onclick = () => this.playChannelAndBlink(i);
        }
    }

    //Plays playback and blinks play button
    public playChannelAndBlink(channel: number): void {
        let playInterval;
        if (!this.recorder.isPlaying()) {
            this.recorder.playChannel(channel);
            playInterval = setInterval(() => this.blinkPlay(channel), 500);
            this.recorder.setPlayInterval(playInterval);
            return;
        }
        clearInterval(playInterval);
        // this.recorder.setPlayInterval(setInterval(() => this.blinkPlay(channel), 500));
        // this.recorder.playChannel(channel);
    }

    //blinks play button
    public blinkPlay(channel: number): void {
        let initialClass = "btn-outline-primary";
        let changeClass = "btn-primary";
        this.channels.get(channel).getPlayButton().classList.remove(initialClass);
        this.channels.get(channel).getPlayButton().classList.add(changeClass);
        setTimeout(() => {
            this.channels.get(channel).getPlayButton().classList.remove(changeClass);
            this.channels.get(channel).getPlayButton().classList.add(initialClass);
        }, 250);
    }

    //Recording event
    public recordAction(ev: MouseEvent, chNr: number) : void {
        if (!this.recorder.isRecording()) {
            this.recorder.startRecording(chNr, ev.timeStamp);
            this.interval = setInterval(() => this.blinkRecordButton(chNr), 500);
            return;
        }
        clearInterval(this.interval);
        this.recorder.stopRecording(ev.timeStamp);
        this.channels.get(chNr).getStates().recordAdded();
        this.refreshStates(chNr);
    }

    //Blinking record button
    public blinkRecordButton(channelNr: number): void {
        let initialClass = "btn-outline-danger";
        let changeClass = "btn-danger";
        this.channels.get(channelNr).getRecordButton().classList.remove(initialClass);
        this.channels.get(channelNr).getRecordButton().classList.add(changeClass);
        setTimeout(() => {
            this.channels.get(channelNr).getRecordButton().classList.remove(changeClass);
            this.channels.get(channelNr).getRecordButton().classList.add(initialClass);
        }, 100);
    }

    //Blink and play beatbox button
    public blinkAndPlay(key: string) {
        this.blinkBeatboxButton(key);
        this.player.play(key);
    }

    //key events
    public onKeyDown(ev: KeyboardEvent ) : void {
        let key : string = ev.key;
        if (this.buttonMap.has(key)) {
            this.blinkBeatboxButton(key);
            this.player.play(key);
            this.recorder.addEntry(key, ev.timeStamp);
        }
    }

    //sets events for all channel radios
    public setRadioClickedEvent(): void {
        for (let i = 1; i <= 4; i++) {
            let channel: ChannelElements = this.channels.get(i);
            channel.getChannelRadio().onclick = () => this.refreshStates(i);
        }
    }

    //refreshes states of channel elements
    public refreshStates(channelNumber: number): void {
        //stops recording on previous channel
        let recordingChannel = this.recorder.getCurrentRecordingChannel()
        if(this.recorder.isRecording() && recordingChannel != channelNumber) {
            this.recorder.stopRecordingWhithiutTimestamp();
            clearInterval(this.interval);
            this.channels.get(recordingChannel).getStates().recordAdded();
        }
        //activates channel and populates states of elements
        this.channels.get(channelNumber).getStates().activateChannel();
        for (let i = 1; i <= 4; i++) {
            if (i != channelNumber) {
                this.channels.get(i).getStates().deactivateChanel();
            }
            this.setStates(this.channels.get(i));
        }
    }

    //Sets states of channels' elements
    public setStates(elements: ChannelElements): void {
        let disabled = "disabled";
        let statuses = elements.getStates();
        if(statuses.isChannelActive()) {
            elements.getRecordButton().classList.remove(disabled);
            if (statuses.isRecordPresent()) {
                this.switchOnPlayback(elements);
            } else {
                this.switchOffPlayback(elements);
            }
        } else {
            elements.getRecordButton().classList.add(disabled);
            if (statuses.isRecordPresent()) {
                this.switchOnPlayback(elements);
            } else {
                this.switchOffPlayback(elements);
            }
        }
    }

    //Switches off playback buttons (play, loop)
    private switchOffPlayback(webElements: ChannelElements): void {
        webElements.getPlayButton().classList.add('disabled');
        webElements.getLoopLabel().classList.add('disabled');
    }
    
    //Switches on playback buttons (play, loop)
    private switchOnPlayback(webElements: ChannelElements): void {
        webElements.getPlayButton().classList.remove('disabled');
        webElements.getLoopLabel().classList.remove('disabled');
    }

    
}

class Player {
    private clap : HTMLAudioElement = document.querySelector('[data-sound="clap"]');
    private boom : HTMLAudioElement = document.querySelector('[data-sound="boom"]');
    private hihat : HTMLAudioElement = document.querySelector('[data-sound="hihat"]');
    private kick : HTMLAudioElement = document.querySelector('[data-sound="kick"]');
    private openhat : HTMLAudioElement = document.querySelector('[data-sound="openhat"]');
    private ride : HTMLAudioElement = document.querySelector('[data-sound="ride"]');
    private snare : HTMLAudioElement = document.querySelector('[data-sound="snare"]');
    private tink : HTMLAudioElement = document.querySelector('[data-sound="tink"]');
    private tom : HTMLAudioElement = document.querySelector('[data-sound="tom"]');
    private audioMap = new Map();
    private playInterval: number;
    constructor() {
        this.audioMap.set("7", this.boom);
        this.audioMap.set("8", this.clap);
        this.audioMap.set("9", this.hihat);
        this.audioMap.set("4", this.kick);
        this.audioMap.set("5", this.openhat);
        this.audioMap.set("6", this.ride);
        this.audioMap.set("1", this.snare);
        this.audioMap.set("2", this.tink);
        this.audioMap.set("3", this.tom);
    }

    public play(key: string): void {
        if (!this.audioMap.has(key)) {
            clearInterval(this.playInterval);
            return;
        }
        let audio = this.audioMap.get(key);
        audio.currentTime = 0;
        audio.play();
    }

    public setPlayInterval(interval: number):void {
        this.playInterval = interval;
    }

}

class ChannelState {
    private channelChecked :boolean;
    private recordPresent: boolean;
    private looping: boolean;

    constructor(channel: boolean, record: boolean, loop: boolean) {
        this.channelChecked = channel;
        this.recordPresent = record;
        this.looping = loop;
    }

    public isChannelActive() : boolean {
        return this.channelChecked;
    }

    public deactivateChanel(): void {
        this.channelChecked = false;
    }

    public activateChannel(): void {
        this.channelChecked = true;
    }

    public recordAdded(): void {
        this.recordPresent = true;
    }

    public isRecordPresent(): boolean {
        return this.recordPresent;
    }

    public setLooper(setter: boolean): void {
        this.looping = setter;
    }

    public isLooped(): boolean {
        return this.looping
    }
}

class ChannelElements {
    private channelRadio: HTMLInputElement;
    private recordButton: HTMLButtonElement;
    private playButton: HTMLButtonElement;
    private loopCheckbox: HTMLInputElement;
    private loopLabel: HTMLLabelElement;
    private states: ChannelState; 
    constructor(nr: number) {
        this.channelRadio = document.querySelector(`#channel-radio${nr}`)
        this.recordButton = document.querySelector(`#rec${nr}`)
        this.playButton = document.querySelector(`#play${nr}`)
        this.loopCheckbox = document.querySelector(`#loop${nr}`)
        this.loopLabel = document.querySelector(`#loop-label${nr}`)
    }

    public setStates(states: ChannelState): void {
        this.states = states;
    }

    public getStates(): ChannelState {
        return this.states;
    }

    public getChannelRadio(): HTMLInputElement {
        return this.channelRadio;
    }

    public getRecordButton(): HTMLButtonElement {
        return this.recordButton;
    }

    public getPlayButton(): HTMLButtonElement {
        return this.playButton;
    }

    public getLoopCheckbox(): HTMLInputElement {
        return this.loopCheckbox;
    }

    public getLoopLabel(): HTMLLabelElement {
        return this.loopLabel;
    }
}

class Recorder {

    private player: Player;
    private records = new Map();
    private startTime: number = 0;
    private currentRecordChannel: number = 0;
    private playing: boolean = false;
    private playInterval: number;

    constructor(player: Player) {
        this.player = player;
    }

    public isRecording(): boolean {
        return this.startTime != 0;
    }

    public isPlaying(): boolean {
        return this.playing;
    }

    public startRecording(ch: number, timestamp: number): void {
        this.currentRecordChannel = ch;
        this.startTime = timestamp;
        this.records.set(ch, [])
    }

    public stopRecording(timeStamp: number): void {
        this.addEntry("", timeStamp);
        this.startTime = 0;
        this.currentRecordChannel = 0;

    }

    public stopRecordingWhithiutTimestamp() : void {
        this.addEntry("", 0);
        this.startTime = 0;
        this.currentRecordChannel = 0;
    }

    public addEntry(key: string, timestamp: number) {
        let time = timestamp - this.startTime;
        this.records.get(this.currentRecordChannel).push({key, time});
    }

    public getCurrentRecordingChannel(): number {
        return this.currentRecordChannel;
    }

    public playChannel(channel: number): void {
        this.records.get(channel).forEach(sound => {
            setTimeout(() => this.player.play(sound.key), sound.time)         
        });
        console.log("finished");
        // clearInterval(this.playInterval);
    }

    public setPlayInterval(interval: number) {
        this.player.setPlayInterval(interval);
    }
}

const beetboxApp = new BeatboxApp();