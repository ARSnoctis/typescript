class BeatboxApp {
    constructor() {
        this.startApp();
    }
    startApp() {
        this.player = new Player();
        this.recorder = new Recorder(this.player);
        this.ui = new UI(this.player, this.recorder);
    }
}
class UI {
    constructor(player, recorder) {
        this.boom = document.querySelector("button#boom");
        this.clap = document.querySelector("button#clap");
        this.hihat = document.querySelector("button#hihat");
        this.kick = document.querySelector("button#kick");
        this.openhat = document.querySelector("button#openhat");
        this.ride = document.querySelector("button#ride");
        this.snare = document.querySelector("button#snare");
        this.tink = document.querySelector("button#tink");
        this.tom = document.querySelector("button#tom");
        this.buttonMap = new Map();
        //recording elements
        this.channels = new Map();
        this.player = player;
        this.recorder = recorder;
        this.buttonMap.set("7", this.boom);
        this.buttonMap.set("8", this.clap);
        this.buttonMap.set("9", this.hihat);
        this.buttonMap.set("4", this.kick);
        this.buttonMap.set("5", this.openhat);
        this.buttonMap.set("6", this.ride);
        this.buttonMap.set("1", this.snare);
        this.buttonMap.set("2", this.tink);
        this.buttonMap.set("3", this.tom);
        this.collectChannelControls();
        this.setKeyEvents();
        this.refreshStates(1); //set innitial state and refresh controls
        this.setRadioClickedEvent();
    }
    //collects channels' controlls
    collectChannelControls() {
        for (let i = 1; i <= 4; i++) {
            let channel = new ChannelElements(i);
            let states = new ChannelState(false, false, false);
            channel.setStates(states);
            this.channels.set(i, channel);
        }
    }
    //blinks beatbox buton event
    blinkBeatboxButton(key) {
        let button = this.buttonMap.get(key);
        button.classList.remove("btn-outline-primary");
        button.classList.add("btn-pressed");
        setTimeout(() => {
            button.classList.remove("btn-pressed");
            button.classList.add("btn-outline-primary");
        }, 50);
    }
    //set document events and for record buttons
    setKeyEvents() {
        document.addEventListener('keypress', (ev) => this.onKeyDown(ev));
        for (let entry of this.buttonMap.entries()) {
            entry[1].onclick = () => this.blinkAndPlay(entry[0]);
        }
        for (let i = 1; i <= 4; i++) {
            this.channels.get(i).getRecordButton().onclick = (ev) => this.recordAction(ev, i);
            this.channels.get(i).getPlayButton().onclick = () => this.playChannelAndBlink(i);
        }
    }
    //Plays playback and blinks play button
    playChannelAndBlink(channel) {
        let playInterval;
        if (!this.recorder.isPlaying()) {
            this.recorder.playChannel(channel);
            playInterval = setInterval(() => this.blinkPlay(channel), 500);
            this.recorder.setPlayInterval(playInterval);
            return;
        }
        clearInterval(playInterval);
        // this.recorder.setPlayInterval(setInterval(() => this.blinkPlay(channel), 500));
        // this.recorder.playChannel(channel);
    }
    //blinks play button
    blinkPlay(channel) {
        let initialClass = "btn-outline-primary";
        let changeClass = "btn-primary";
        this.channels.get(channel).getPlayButton().classList.remove(initialClass);
        this.channels.get(channel).getPlayButton().classList.add(changeClass);
        setTimeout(() => {
            this.channels.get(channel).getPlayButton().classList.remove(changeClass);
            this.channels.get(channel).getPlayButton().classList.add(initialClass);
        }, 250);
    }
    //Recording event
    recordAction(ev, chNr) {
        if (!this.recorder.isRecording()) {
            this.recorder.startRecording(chNr, ev.timeStamp);
            this.interval = setInterval(() => this.blinkRecordButton(chNr), 500);
            return;
        }
        clearInterval(this.interval);
        this.recorder.stopRecording(ev.timeStamp);
        this.channels.get(chNr).getStates().recordAdded();
        this.refreshStates(chNr);
    }
    //Blinking record button
    blinkRecordButton(channelNr) {
        let initialClass = "btn-outline-danger";
        let changeClass = "btn-danger";
        this.channels.get(channelNr).getRecordButton().classList.remove(initialClass);
        this.channels.get(channelNr).getRecordButton().classList.add(changeClass);
        setTimeout(() => {
            this.channels.get(channelNr).getRecordButton().classList.remove(changeClass);
            this.channels.get(channelNr).getRecordButton().classList.add(initialClass);
        }, 100);
    }
    //Blink and play beatbox button
    blinkAndPlay(key) {
        this.blinkBeatboxButton(key);
        this.player.play(key);
    }
    //key events
    onKeyDown(ev) {
        let key = ev.key;
        if (this.buttonMap.has(key)) {
            this.blinkBeatboxButton(key);
            this.player.play(key);
            this.recorder.addEntry(key, ev.timeStamp);
        }
    }
    //sets events for all channel radios
    setRadioClickedEvent() {
        for (let i = 1; i <= 4; i++) {
            let channel = this.channels.get(i);
            channel.getChannelRadio().onclick = () => this.refreshStates(i);
        }
    }
    //refreshes states of channel elements
    refreshStates(channelNumber) {
        //stops recording on previous channel
        let recordingChannel = this.recorder.getCurrentRecordingChannel();
        if (this.recorder.isRecording() && recordingChannel != channelNumber) {
            this.recorder.stopRecordingWhithiutTimestamp();
            clearInterval(this.interval);
            this.channels.get(recordingChannel).getStates().recordAdded();
        }
        //activates channel and populates states of elements
        this.channels.get(channelNumber).getStates().activateChannel();
        for (let i = 1; i <= 4; i++) {
            if (i != channelNumber) {
                this.channels.get(i).getStates().deactivateChanel();
            }
            this.setStates(this.channels.get(i));
        }
    }
    //Sets states of channels' elements
    setStates(elements) {
        let disabled = "disabled";
        let statuses = elements.getStates();
        if (statuses.isChannelActive()) {
            elements.getRecordButton().classList.remove(disabled);
            if (statuses.isRecordPresent()) {
                this.switchOnPlayback(elements);
            }
            else {
                this.switchOffPlayback(elements);
            }
        }
        else {
            elements.getRecordButton().classList.add(disabled);
            if (statuses.isRecordPresent()) {
                this.switchOnPlayback(elements);
            }
            else {
                this.switchOffPlayback(elements);
            }
        }
    }
    //Switches off playback buttons (play, loop)
    switchOffPlayback(webElements) {
        webElements.getPlayButton().classList.add('disabled');
        webElements.getLoopLabel().classList.add('disabled');
    }
    //Switches on playback buttons (play, loop)
    switchOnPlayback(webElements) {
        webElements.getPlayButton().classList.remove('disabled');
        webElements.getLoopLabel().classList.remove('disabled');
    }
}
class Player {
    constructor() {
        this.clap = document.querySelector('[data-sound="clap"]');
        this.boom = document.querySelector('[data-sound="boom"]');
        this.hihat = document.querySelector('[data-sound="hihat"]');
        this.kick = document.querySelector('[data-sound="kick"]');
        this.openhat = document.querySelector('[data-sound="openhat"]');
        this.ride = document.querySelector('[data-sound="ride"]');
        this.snare = document.querySelector('[data-sound="snare"]');
        this.tink = document.querySelector('[data-sound="tink"]');
        this.tom = document.querySelector('[data-sound="tom"]');
        this.audioMap = new Map();
        this.audioMap.set("7", this.boom);
        this.audioMap.set("8", this.clap);
        this.audioMap.set("9", this.hihat);
        this.audioMap.set("4", this.kick);
        this.audioMap.set("5", this.openhat);
        this.audioMap.set("6", this.ride);
        this.audioMap.set("1", this.snare);
        this.audioMap.set("2", this.tink);
        this.audioMap.set("3", this.tom);
    }
    play(key) {
        if (!this.audioMap.has(key)) {
            clearInterval(this.playInterval);
            return;
        }
        let audio = this.audioMap.get(key);
        audio.currentTime = 0;
        audio.play();
    }
    setPlayInterval(interval) {
        this.playInterval = interval;
    }
}
class ChannelState {
    constructor(channel, record, loop) {
        this.channelChecked = channel;
        this.recordPresent = record;
        this.looping = loop;
    }
    isChannelActive() {
        return this.channelChecked;
    }
    deactivateChanel() {
        this.channelChecked = false;
    }
    activateChannel() {
        this.channelChecked = true;
    }
    recordAdded() {
        this.recordPresent = true;
    }
    isRecordPresent() {
        return this.recordPresent;
    }
    setLooper(setter) {
        this.looping = setter;
    }
    isLooped() {
        return this.looping;
    }
}
class ChannelElements {
    constructor(nr) {
        this.channelRadio = document.querySelector(`#channel-radio${nr}`);
        this.recordButton = document.querySelector(`#rec${nr}`);
        this.playButton = document.querySelector(`#play${nr}`);
        this.loopCheckbox = document.querySelector(`#loop${nr}`);
        this.loopLabel = document.querySelector(`#loop-label${nr}`);
    }
    setStates(states) {
        this.states = states;
    }
    getStates() {
        return this.states;
    }
    getChannelRadio() {
        return this.channelRadio;
    }
    getRecordButton() {
        return this.recordButton;
    }
    getPlayButton() {
        return this.playButton;
    }
    getLoopCheckbox() {
        return this.loopCheckbox;
    }
    getLoopLabel() {
        return this.loopLabel;
    }
}
class Recorder {
    constructor(player) {
        this.records = new Map();
        this.startTime = 0;
        this.currentRecordChannel = 0;
        this.playing = false;
        this.player = player;
    }
    isRecording() {
        return this.startTime != 0;
    }
    isPlaying() {
        return this.playing;
    }
    startRecording(ch, timestamp) {
        this.currentRecordChannel = ch;
        this.startTime = timestamp;
        this.records.set(ch, []);
    }
    stopRecording(timeStamp) {
        this.addEntry("", timeStamp);
        this.startTime = 0;
        this.currentRecordChannel = 0;
    }
    stopRecordingWhithiutTimestamp() {
        this.addEntry("", 0);
        this.startTime = 0;
        this.currentRecordChannel = 0;
    }
    addEntry(key, timestamp) {
        let time = timestamp - this.startTime;
        this.records.get(this.currentRecordChannel).push({ key, time });
    }
    getCurrentRecordingChannel() {
        return this.currentRecordChannel;
    }
    playChannel(channel) {
        this.records.get(channel).forEach(sound => {
            setTimeout(() => this.player.play(sound.key), sound.time);
        });
        console.log("finished");
        // clearInterval(this.playInterval);
    }
    setPlayInterval(interval) {
        this.player.setPlayInterval(interval);
    }
}
const beetboxApp = new BeatboxApp();
