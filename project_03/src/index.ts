class WeatherApp {
    ui: UI;
    storage: WebStorage;
    apiCaller: ApiCaller;
    callback: CallbackManager;
    constructor() {
        this.startApp();
    }
    startApp() {
        this.callback = new CallbackManager();
        this.storage = new WebStorage(this.callback);
        this.apiCaller = new ApiCaller(this.storage);
        this.storage.addApiCaller(this.apiCaller);
        this.ui = new UI(this.apiCaller, this.storage);
        this.callback.addSubscriber(this.ui);
        this.storage.readStorage();
       
    }
}

class UI implements CallbackReciever {
    searchButton: HTMLButtonElement = document.querySelector("button#search");
    input: HTMLInputElement = document.querySelector("input");
    cityCard: HTMLDivElement = document.querySelector("#city_card");
    cardPanel: HTMLDivElement = document.querySelector("#card_panel");
    heroCard: HTMLDivElement = document.querySelector("#hero_panel");
    heroPanel: HTMLDivElement = document.querySelector("#forecast_panel");
    apicaller: ApiCaller;
    storage: WebStorage;


    constructor(apicaller: ApiCaller, storage: WebStorage) {
        this.apicaller = apicaller;
        this.storage = storage;
        this.searchButton.addEventListener("click", (e: Event) => this.readCityInput(e));
    }


    update(data: any): void {
        this.createPanelItem(data);
    }

    updateHero(city:string, data: any): void {
        this.createHeroItem(city, data);
    }

    readCityInput(e: Event) {
        e.preventDefault();
        this.apicaller.getCurrentWeather(this.input.value);
        this.input.value = null;
    }

    createPanelItem(data: any) : void {
        
        var card = this.cityCard.cloneNode(true) as HTMLDivElement;
        card.id = data.name;
        card.querySelector("#city").innerHTML = data.name;
        card.querySelector("#temp").innerHTML = `${Math.round(data.main.temp)} °C`;
        card.querySelector("#air-press").innerHTML = `${data.main.pressure}hPa`;
        card.querySelector("#hum").innerHTML = `${data.main.humidity}%`;
        card.querySelector("img").setAttribute("src",`http://openweathermap.org/img/wn/${data.weather[0].icon}@2x.png`);
        card.classList.remove("d-none");
        this.cardPanel.appendChild(card);
        card.querySelector("#more").addEventListener("click", () => {
            this.apicaller.getForecastWeather(data.name, data.coord.lat, data.coord.lon);
        });
        card.querySelector("button").addEventListener("click", () => {
            card.remove();
            this.storage.remove(data.name);
        });
    }

    createHeroItem(city: string, data: any) : void {
        var panel = this.heroPanel.cloneNode(true) as HTMLDivElement;
        var citydata = this.storage.get(city);
        panel.querySelector("#city").innerHTML = citydata.name;
        var todayCard = panel.querySelector("#today-weather-data");
        todayCard.querySelector("#temp").innerHTML = `${Math.round(citydata.main.temp)} °C`;
        todayCard.querySelector("#air-press").innerHTML = `${citydata.main.pressure}hPa`;
        todayCard.querySelector("#hum").innerHTML = `${citydata.main.humidity}%`;
        todayCard.querySelector("img").setAttribute("src",`http://openweathermap.org/img/wn/${citydata.weather[0].icon}@2x.png`);
        
        var tommorowData = data.daily[0];
        var tomorrowCard = panel.querySelector("#tomorrow-weather-data");
        tomorrowCard.querySelector("#temp").innerHTML = `${Math.round(tommorowData.temp.day)} °C`;
        tomorrowCard.querySelector("#air-press").innerHTML = `${tommorowData.pressure}hPa`;
        tomorrowCard.querySelector("#hum").innerHTML = `${tommorowData.humidity}%`;
        tomorrowCard.querySelector("img").setAttribute("src",`http://openweathermap.org/img/wn/${tommorowData.weather[0].icon}@2x.png`);

        var dtommorowData = data.daily[1];
        var dtomorrowCard = panel.querySelector("#dayatomorrow-weather-data");
        dtomorrowCard.querySelector("#temp").innerHTML = `${Math.round(dtommorowData.temp.day)} °C`;
        dtomorrowCard.querySelector("#air-press").innerHTML = `${dtommorowData.pressure}hPa`;
        dtomorrowCard.querySelector("#hum").innerHTML = `${dtommorowData.humidity}%`;
        dtomorrowCard.querySelector("img").setAttribute("src",`http://openweathermap.org/img/wn/${dtommorowData.weather[0].icon}@2x.png`);
        panel.classList.remove("d-none");
        this.heroCard.appendChild(panel);
        panel.querySelector("button").addEventListener("click", () => {
            panel.remove();
            this.storage.removeForcast(city);
        });
    }
}

class WebStorage {


    cityArray: string[] = [];
    cityForecast: string[] = [];
    citiesMap = new Map();
    callback: CallbackManager;
    apiCaller: ApiCaller;
    constructor(callback: CallbackManager){
        this.callback = callback;
    }

    readStorage() : void {
        if(window.localStorage.getItem("cities")) {
            let storagedItem: string = window.localStorage.getItem("cities");
            let storageArray: string[] = [];
            storageArray = JSON.parse(storagedItem)!;
            storageArray.forEach( (element: string) => {
                this.apiCaller.getCurrentWeather(element);
            })
        }
    }

    add(data: any): void {
        if (this.cityArray.includes(data.name)) {
            alert(`${data.name} is already on list`);
            return;
        }
        this.citiesMap.set(data.name, data);
        this.cityArray.push(data.name);
        this.callback.update(data);
        window.localStorage.setItem("cities", JSON.stringify(this.cityArray));
    }

    addForecast(city: string, data: any): void {
        if (this.cityForecast.includes(city)) {
            alert(`${city} is already on main list`);
            return;
        }
        this.cityForecast.push(city);
        this.callback.updateHero(city, data);
    }

    get(city: string): any {
        return this.citiesMap.get(city);
    }

    remove(city: any) {
        this.citiesMap.delete(city);
        this.cityArray = this.cityArray.filter(item => item != city);
        window.localStorage.setItem("cities", JSON.stringify(this.cityArray));
    }

    removeForcast(city: string) {
        this.cityForecast = this.cityArray.filter(item => item != city);
    }

    addApiCaller(apiCaller: ApiCaller) {
        this.apiCaller = apiCaller;
    }

}

class CallbackManager {
    subscribers: CallbackReciever[] = [];

    constructor() {
    }

    addSubscriber(subscriber: CallbackReciever) {
        this.subscribers.push(subscriber);
    }

    update(data: any) {
        for (let subscriber of this.subscribers) {
            subscriber.update(data);
        }
    }

    updateHero(city: string, data: any) {
        for (let subscriber of this.subscribers) {
            subscriber.updateHero(city, data);
        }
    }

}

interface CallbackReciever {
    update(data: any): void;
    updateHero(city:string, data: any):void;
}

class ApiCaller {
    key: string = "4aec22aff4fa46e5385d7d9384de57fd";
    storage: WebStorage;
    constructor(storage: WebStorage) {
        this.storage  = storage;
    }

    getCurrentWeather = async (cityName: string): Promise<void> => {
        fetch(`http://api.openweathermap.org/data/2.5/weather?q=${cityName}&units=metric&appid=${this.key}`)
            .then(response => response.json())
            .then(data => {
                this.storage.add(data);
            })
            .catch(err => {
                console.error(err)
                alert("Incorrect city, please type existing city.");
            });
    }

    getForecastWeather = async (city: string, latitude: number, longitude: number): Promise<void> => {
        fetch(`https://api.openweathermap.org/data/2.5/onecall?lat=${latitude}&lon=${longitude}&units=metric&exclude=current,minutely,hourly,alerts&appid=${this.key}`)
            .then(res => res.json())
            .then(data => {
                this.storage.addForecast(city, data);  
            })
            .catch(err => console.error(err));
    }
}

const weatherApp = new WeatherApp();