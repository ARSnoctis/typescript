class WeatherApp {
    constructor() {
        this.startApp();
    }
    startApp() {
        this.callback = new CallbackManager();
        this.storage = new WebStorage(this.callback);
        this.apiCaller = new ApiCaller(this.storage);
        this.storage.addApiCaller(this.apiCaller);
        this.ui = new UI(this.apiCaller, this.storage);
        this.callback.addSubscriber(this.ui);
        this.storage.readStorage();
    }
}
class UI {
    constructor(apicaller, storage) {
        this.searchButton = document.querySelector("button#search");
        this.input = document.querySelector("input");
        this.cityCard = document.querySelector("#city_card");
        this.cardPanel = document.querySelector("#card_panel");
        this.heroCard = document.querySelector("#hero_panel");
        this.heroPanel = document.querySelector("#forecast_panel");
        this.apicaller = apicaller;
        this.storage = storage;
        this.searchButton.addEventListener("click", (e) => this.readCityInput(e));
    }
    update(data) {
        this.createPanelItem(data);
    }
    updateHero(city, data) {
        this.createHeroItem(city, data);
    }
    readCityInput(e) {
        e.preventDefault();
        this.apicaller.getCurrentWeather(this.input.value);
        this.input.value = null;
    }
    createPanelItem(data) {
        var card = this.cityCard.cloneNode(true);
        card.id = data.name;
        card.querySelector("#city").innerHTML = data.name;
        card.querySelector("#temp").innerHTML = `${Math.round(data.main.temp)} °C`;
        card.querySelector("#air-press").innerHTML = `${data.main.pressure}hPa`;
        card.querySelector("#hum").innerHTML = `${data.main.humidity}%`;
        card.querySelector("img").setAttribute("src", `http://openweathermap.org/img/wn/${data.weather[0].icon}@2x.png`);
        card.classList.remove("d-none");
        this.cardPanel.appendChild(card);
        card.querySelector("#more").addEventListener("click", () => {
            this.apicaller.getForecastWeather(data.name, data.coord.lat, data.coord.lon);
        });
        card.querySelector("button").addEventListener("click", () => {
            card.remove();
            this.storage.remove(data.name);
        });
    }
    createHeroItem(city, data) {
        var panel = this.heroPanel.cloneNode(true);
        var citydata = this.storage.get(city);
        panel.querySelector("#city").innerHTML = citydata.name;
        var todayCard = panel.querySelector("#today-weather-data");
        todayCard.querySelector("#temp").innerHTML = `${Math.round(citydata.main.temp)} °C`;
        todayCard.querySelector("#air-press").innerHTML = `${citydata.main.pressure}hPa`;
        todayCard.querySelector("#hum").innerHTML = `${citydata.main.humidity}%`;
        todayCard.querySelector("img").setAttribute("src", `http://openweathermap.org/img/wn/${citydata.weather[0].icon}@2x.png`);
        var tommorowData = data.daily[0];
        var tomorrowCard = panel.querySelector("#tomorrow-weather-data");
        tomorrowCard.querySelector("#temp").innerHTML = `${Math.round(tommorowData.temp.day)} °C`;
        tomorrowCard.querySelector("#air-press").innerHTML = `${tommorowData.pressure}hPa`;
        tomorrowCard.querySelector("#hum").innerHTML = `${tommorowData.humidity}%`;
        tomorrowCard.querySelector("img").setAttribute("src", `http://openweathermap.org/img/wn/${tommorowData.weather[0].icon}@2x.png`);
        var dtommorowData = data.daily[1];
        var dtomorrowCard = panel.querySelector("#dayatomorrow-weather-data");
        dtomorrowCard.querySelector("#temp").innerHTML = `${Math.round(dtommorowData.temp.day)} °C`;
        dtomorrowCard.querySelector("#air-press").innerHTML = `${dtommorowData.pressure}hPa`;
        dtomorrowCard.querySelector("#hum").innerHTML = `${dtommorowData.humidity}%`;
        dtomorrowCard.querySelector("img").setAttribute("src", `http://openweathermap.org/img/wn/${dtommorowData.weather[0].icon}@2x.png`);
        panel.classList.remove("d-none");
        this.heroCard.appendChild(panel);
        panel.querySelector("button").addEventListener("click", () => {
            panel.remove();
            this.storage.removeForcast(city);
        });
    }
}
class WebStorage {
    constructor(callback) {
        this.cityArray = [];
        this.cityForecast = [];
        this.citiesMap = new Map();
        this.callback = callback;
    }
    readStorage() {
        if (window.localStorage.getItem("cities")) {
            let storagedItem = window.localStorage.getItem("cities");
            let storageArray = [];
            storageArray = JSON.parse(storagedItem);
            storageArray.forEach((element) => {
                this.apiCaller.getCurrentWeather(element);
            });
        }
    }
    add(data) {
        if (this.cityArray.includes(data.name)) {
            alert(`${data.name} is already on list`);
            return;
        }
        this.citiesMap.set(data.name, data);
        this.cityArray.push(data.name);
        this.callback.update(data);
        window.localStorage.setItem("cities", JSON.stringify(this.cityArray));
    }
    addForecast(city, data) {
        if (this.cityForecast.includes(city)) {
            alert(`${city} is already on main list`);
            return;
        }
        this.cityForecast.push(city);
        this.callback.updateHero(city, data);
    }
    get(city) {
        return this.citiesMap.get(city);
    }
    remove(city) {
        this.citiesMap.delete(city);
        this.cityArray = this.cityArray.filter(item => item != city);
        window.localStorage.setItem("cities", JSON.stringify(this.cityArray));
    }
    removeForcast(city) {
        this.cityForecast = this.cityArray.filter(item => item != city);
    }
    addApiCaller(apiCaller) {
        this.apiCaller = apiCaller;
    }
}
class CallbackManager {
    constructor() {
        this.subscribers = [];
    }
    addSubscriber(subscriber) {
        this.subscribers.push(subscriber);
    }
    update(data) {
        for (let subscriber of this.subscribers) {
            subscriber.update(data);
        }
    }
    updateHero(city, data) {
        for (let subscriber of this.subscribers) {
            subscriber.updateHero(city, data);
        }
    }
}
class ApiCaller {
    constructor(storage) {
        this.key = "4aec22aff4fa46e5385d7d9384de57fd";
        this.getCurrentWeather = async (cityName) => {
            fetch(`http://api.openweathermap.org/data/2.5/weather?q=${cityName}&units=metric&appid=${this.key}`)
                .then(response => response.json())
                .then(data => {
                this.storage.add(data);
            })
                .catch(err => {
                console.error(err);
                alert("Incorrect city, please type existing city.");
            });
        };
        this.getForecastWeather = async (city, latitude, longitude) => {
            fetch(`https://api.openweathermap.org/data/2.5/onecall?lat=${latitude}&lon=${longitude}&units=metric&exclude=current,minutely,hourly,alerts&appid=${this.key}`)
                .then(res => res.json())
                .then(data => {
                this.storage.addForecast(city, data);
            })
                .catch(err => console.error(err));
        };
        this.storage = storage;
    }
}
const weatherApp = new WeatherApp();
